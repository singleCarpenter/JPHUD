
Pod::Spec.new do |s|


  s.name         = "JPHUD"

  s.version      = "1.0.1"

  s.summary      = "优化图片加载方式，修复其他项目引入JPHUD无法加载资源bug"

  s.description  = <<-DESC
              一款自定义加载等待框
                   DESC

  s.homepage     = "https://gitee.com/chaoqianzoubutingxie/JPHUD"

  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author       = { "Carpenter" => "158287481@qq.com" }

  s.platform     = :ios, "9.0"

  s.source       = { :git => "https://gitee.com/chaoqianzoubutingxie/JPHUD.git", :tag => s.version }

  s.source_files  = "JPHUD/JPHUD.{h,m}"

  s.public_header_files = "JPHUD/JPHUD.h"

  s.resources = "JPHUD/*.bundle"

  s.frameworks = "UIKit", "Foundation"

  s.requires_arc = true

  s.dependency 'Masonry'
  s.dependency 'pop'

end
