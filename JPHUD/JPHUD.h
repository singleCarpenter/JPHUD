//
//  JPHUD.h
//  JPHUD
//
//  Created by carpenter.Li on 2017/8/14.
//  Copyright © 2017年 Carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 类型

 - JPProgressHUDMode_Text: 文本
 - JPProgressHUDMode_Success: 成功
 - JPProgressHUDMode_Error: 失败
 - JPProgressHUDMode_Progress: 进度
 - JPProgressHUDMode_Custom: 预留
 */
typedef NS_ENUM(NSInteger ,JPProgressHUDMode) {
    JPProgressHUDMode_Text,
    JPProgressHUDMode_Success,
    JPProgressHUDMode_Error,
    JPProgressHUDMode_Progress,
    JPProgressHUDMode_Custom,//预留
};

/**
 展示位置

 - JPProgressHUDShowType_Center: 中心
 - JPProgressHUDShowType_Bottom: 底部
 */
typedef NS_ENUM(NSInteger, JPProgressHUDShowType) {
    JPProgressHUDShowType_Center,
    JPProgressHUDShowType_Bottom,
};

/**
 背景

 - JPProgressHUDBackgroundStyle_Clear: 不加载
 - JPProgressHUDBackgroundStyle_gray: 半透明
 */
typedef NS_ENUM(NSInteger, JPProgressHUDBackgroundStyle) {
    JPProgressHUDBackgroundStyle_Clear,
    JPProgressHUDBackgroundStyle_gray,
};


@interface JPProgressContentView : UIView

/**
 展示样式
 */
@property (nonatomic ,assign) JPProgressHUDMode mode;


/**
 初始化视图
 
 @param title 说明文本
 @param mode 展示样式
 @return JPProgressContentView
 */
- (instancetype)initWithTitle:(NSString*)title mode:(JPProgressHUDMode)mode;


@end

@interface JPProgressBackgroundView : UIView


@property (nonatomic ,assign) JPProgressHUDBackgroundStyle backgroundStyle;

/**
 初始化视图
 
 @param style 展示样式
 @return JPProgressContentView
 */
-(instancetype)initWithStyle:(JPProgressHUDBackgroundStyle)style;


@end


@interface JPHUD : UIView


/**
 遮罩视图
 */
@property (nonatomic ,strong) JPProgressBackgroundView * backgroundView;


/**
 展示视图
 */
@property (nonatomic ,strong) JPProgressContentView * contentView;

/**
 动画完成回调
 */
@property (nonatomic ,copy) void(^hudHideComplete)(void);

/**
 初始化视图
 
 @param view 依赖视图（夫视图）
 @param title 说明文本
 @param continuousTimeInterval 视图展示时间
 @param contentViewMode 展示样式
 @param showType 展示位置
 @param backgroundStyle 背景样式
 @return JPProgressHUD
 */
-(instancetype)initWithView:(UIView*)view title:(NSString*)title continuousTimeInterval:(NSTimeInterval)continuousTimeInterval contentViewMode:(JPProgressHUDMode)contentViewMode showType:(JPProgressHUDShowType)showType backgroundStyle:(JPProgressHUDBackgroundStyle)backgroundStyle;



/**
 加载文字说明视图
 
 @param title 说明文字
 @param view 依赖视图
 @return JPProgressHUD
 */
+ (instancetype) showText:(NSString*)title view:(UIView*)view;
+ (instancetype) showText:(NSString*)title;


/**
 加载等待视图
 
 @param title 说明文字
 @param view 依赖视图
 @return JPProgressHUD
 */
+ (instancetype) showProgress:(NSString*)title view:(UIView*)view;
+ (instancetype) showProgress:(NSString*)title;
+ (instancetype) showProgress;

/**
 成功视图
 
 @param title 说明文字
 @param view 依赖视图
 @return JPProgressHUD
 */
+ (instancetype) showSuccess:(NSString*)title view:(UIView*)view;
+ (instancetype) showSuccess:(NSString*)title;
+ (instancetype) showSuccess;

/**
 失败视图
 
 @param title 说明文字
 @param view 依赖视图
 @return JPProgressHUD
 */
+ (instancetype) showError:(NSString*)title view:(UIView*)view;
+ (instancetype) showError:(NSString*)title;
+ (instancetype) showError;



/**
 移除视图
 */
+ (void)hideHUD;
+ (void)hideHUDForAnimated:(BOOL)animated;
+ (void)hideHUDInView:(UIView*)view animated:(BOOL)animated;

@end
 

