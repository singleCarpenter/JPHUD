//
//  ViewController.m
//  JPHUDDemo
//
//  Created by Carpenter on 2018/2/9.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "ViewController.h"
#import "JPHUD.h"


@interface ViewController ()



@end

@implementation ViewController

-(void)jp_requestData{
    [super jp_requestData];
    
    [self.tableView reloadData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    JPBaseCell * cell = [JPBaseCell jp_cellForTableView:tableView];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    
    switch (indexPath.row) {
        case 0:
        {
            [JPHUD showText:@"text"];
        }
            break;
        case 1:
        {
            [JPHUD showSuccess];
        }
            break;
        case 2:
        {
            [JPHUD showError];
        }
            break;
        case 3:
        {
            [JPHUD showProgress];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [JPHUD hideHUD];
            });
            
        }
            break;
        case 4:
        {
            
        }
            break;
        default:
            break;
    }
    
}
@end
