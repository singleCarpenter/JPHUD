//
//  AppDelegate.h
//  JPHUDDemo
//
//  Created by Carpenter on 2018/2/9.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

