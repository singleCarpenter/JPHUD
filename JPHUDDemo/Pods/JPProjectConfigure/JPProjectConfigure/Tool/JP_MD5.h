//
//  JP_MD5.h
//  JPProjectConfigureDemo
//
//  Created by mac on 2018/4/28.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JP_MD5 : NSObject

//把字符串加密成32位小写md5字符串
+ (NSString*)md532BitLower:(NSString *)inPutText;

//把字符串加密成32位大写md5字符串
+ (NSString*)md532BitUpper:(NSString*)inPutText;


@end
