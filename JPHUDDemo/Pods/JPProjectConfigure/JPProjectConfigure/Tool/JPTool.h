//
//  JPTool.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/3/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface JPTool : NSObject

/**
 * @brief 切换窗口根视图
 *
 * @param controller 根视图
 */
+(void)changeWindowRootViewController:(UIViewController*)controller;


/**
 * @brief 拨打电话
 *
 * @param mobile 电话号码
 */
+(void)callPhone:(NSString*)mobile;


/**
 转换字体，适配屏幕
 以width=375为标志

 @param fontSize 字体大小
 @return 适配屏幕字体大小
 */
+(CGFloat)newFontSize:(CGFloat)fontSize;


/**
 字段转字符串

 @param dict 原始数据
 @return 转换数据
 */
+(NSString *)convertJsonToString:(NSDictionary *)dict;

/**
 字符串转字典

 @param jsonString 原始数据
 @return 转换数据
 */
+(NSDictionary *)convertStringToJson:(NSString *)jsonString;




@end

/** 用户存储个人信息的键 */
extern NSString * const kToolAccoutLocalDataKey;

@interface JPTool (Accout)

/**
 * @brief 用户是否在登录状态（即判断本地是否有存储的用户信息）
 *
 * @return 登录/未登录
 */
+(BOOL)isLogining;

/**
 * @brief 退出登录（即删除本地存储的用户信息）
 */
+(void)logingout;

/**
 * @brief 获取用户登录时保存的个人信息
 *
 * @return 个人信息(json)
 */
+(id)accoutData;

/**
 * @brief 同步存储用户信息，作为用户是否在登录状态的依据
 *
 * @discussion 一般登录成功调用此接口
 *
 * @param data 用户信息
 *
 * @return 存储成功/失败
 */
+(BOOL)synchronizeStorageAccoutData:(id)data;

@end


/** 默认样式 yyyy-MM-dd HH:mm:ss */
extern NSString * const kTimeStringFormatTypeDefaults;
/** 中文样式 yyyy年MM月dd日 HH时mm分ss秒 */
extern NSString * const kTimeStringFormatTypeChinese;
/** 年月日样式 yyyy/MM/dd */
extern NSString * const kTimeStringFormatTypeYearMonthDay;

@interface JPTool (Time)


/**
 * @brief 获取某一刻时间
 *
 * @param timeType 时间字符样式
 *
 * @param timeInterval 时间戳
 *
 * @return 时间字符
 */
+(NSString *)timeStringWithType:(NSString*)timeType interval:(NSTimeInterval)timeInterval;

@end






/** 本地存储的应用版本号，用来判断用户是否第一次启动应用（包含版本升级后第一次启动） */
extern NSString * const kLocalFirstStartKey;

///验证信息
@interface JPTool (Validate)

/**
 * @brief 是否有齐刘海
 *
 * @return 是/否
 */
+(BOOL)isNotchIphone;

/**
 * @brief 是否第一次启动
 *
 * @return 是/否
 */
+(BOOL)isFirstStart;

/**
 * @brief 验证字符串是否为浮点型
 *
 * @param string 待验证字符串
 *
 * @return 是/否
 */
+ (BOOL)isPureFloat:(NSString *)string;

/**
 * @brief 验证字符串是否包含中文
 *
 * @param string 待验证字符串
 *
 * @return 是/否
 */
+ (BOOL)isChinese:(NSString *)string;

/**
 * @brief 验证字符串是否为手机号码
 *
 * @parma mobile 待校验字符串
 *
 * @return 是/否
 */
+(BOOL)isMobile:(NSString *)mobile;

/**
 * @brief 验证字符串是否符合密码格式
 *
 * @param psw 待验证字符串
 *
 * @return 是/否
 */
+ (BOOL)isQualifiedPsw:(NSString *)psw;

/**
 * @brief 验证字符串是否符合银行卡
 *
 * @param cardNumber 待验证字符串
 *
 * @return 是/否
 */
+ (BOOL)IsBankCard:(NSString *)cardNumber;

/**
 * @brief 验证字符串是否符合身份证
 *
 * @param ID 待验证字符串
 *
 * @return 是/否
 */
+(BOOL)isIDCard:(NSString *)ID;

/**
 * @brief 验证对象是否为空
 *
 * @param sender 待验证对象
 *
 * @return 是/否
 */
+ (BOOL)isBlank:(id)sender;

/**
 * @brief 限制输入框字节
 *
 * @return 超出/未超出
 */
+ (BOOL)isInputText:(NSString *)numString shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string length:(NSInteger)stringLenght;

@end
