//
//  JPProjectConfigure.h
//  JPProjectConfigureDemo
//
//  Created by mac on 2018/7/5.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#ifdef DEBUG
#define NSLog(FORMAT, ...) fprintf(stderr,"%s:%d\t%s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else
#define NSLog(FORMAT, ...) nil
#endif


#ifndef JPProjectConfigure_h
#define JPProjectConfigure_h

//MARK: - Git
//// 只要添加了这个宏，就不用带mas_前缀
//#define MAS_SHORTHAND
//#define MAS_SHORTHAND_GLOBALS
#import <Masonry/Masonry.h>

#import <AFNetworking/AFNetworking.h>

#import <ReactiveObjC/ReactiveObjC.h>

#import <SDWebImage/UIImage+GIF.h>

#import <SDWebImage/UIButton+WebCache.h>

#import <SDWebImage/UIImageView+WebCache.h>

#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

#import <ZYCornerRadius/UIImageView+CornerRadius.h>

//#import <IGListKit/IGListKit.h>

#import <MJExtension/MJExtension.h>

#import <FDFullscreenPopGesture/UINavigationController+FDFullscreenPopGesture.h>

//MARK: - Tool

#import "JPTool.h"

#import "JP_MD5.h"

//MARK: - Constant

#import "JPUtilsMacro.h"

//MARK: - Net

#import "JPNetManager.h"

//MARK: - Category

#import "UIColor+Hex.h"

#import "NSDate+Calendar.h"

#import "UIImage+JPImage.h"

#import "UIImage+Color.h"

#import "NSObject+Parse.h"

#import "NSObject+Params.h"

#import "UIView+BindVM.h"

#import "UIButton+AxcButtonContentLayout.h"

#import "UIButton+AxcButtonCountDown.h"

#import "UIAlertController+JP.h"

#import "UINavigationController+StackItems.h"

//MARK: - BaseClass

#import "JPBaseCell.h"

#import "JPBaseViewController.h"

#import "JPBaseNavigationController.h"

#import "JPBaseTabBarController.h"

#import "JPBaseWebController.h"

#endif /* JPProjectConfigure_h */

