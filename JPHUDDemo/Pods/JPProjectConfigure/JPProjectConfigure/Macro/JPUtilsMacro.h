
//MARK: - 各屏幕大小


#define JP_IPhoneSize_3_5    ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_4_0    ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_4_7    ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_5_5    ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_X      ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_Xr     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_Xs     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define JP_IPhoneSize_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size) : NO)

 
//#define Height_StatusBar ((JP_IPhoneSize_X == YES || JP_IPhoneSize_Xr == YES || JP_IPhoneSize_Xs == YES || JP_IPhoneSize_Xs_Max == YES) ? 44.0 : 20.0)
//#define Height_NavBar ((JP_IPhoneSize_X == YES || JP_IPhoneSize_Xr == YES || JP_IPhoneSize_Xs == YES || JP_IPhoneSize_Xs_Max == YES) ? 88.0 : 64.0)
//#define Height_TabBar ((JP_IPhoneSize_X == YES || JP_IPhoneSize_Xr == YES || JP_IPhoneSize_Xs == YES || JP_IPhoneSize_Xs_Max == YES) ? 83.0 : 49.0)



//MARK: - 获取系统版本
//#define JP_IOS_SYSTEM_VERSION  [[[UIDevice currentDevice] systemVersion] floatValue]

//MARK: - 常量

//屏幕宽度
#define  JPScreenWidth          ([UIScreen mainScreen].bounds.size.width)

//屏幕高度
#define  JPScreenHeight         ([UIScreen mainScreen].bounds.size.height)

//屏幕尺寸
#define  JPScreenBounds         ([UIScreen mainScreen].bounds)

//屏幕缩放因子
#define  JPScreenMultiplie      (JPScreenWidth / 320.0)

//状态栏高度
#define  JPStateBarHeight        (CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]))

//导航视图高度
#define  JPNavBarHeight         44

//导航栏高度（状态栏+导航视图高度）
#define  JPNavControllerHeight  (JPStateBarHeight + JPNavBarHeight)

//tabbar高度
#define  JPTabBarHeight ((JPStateBarHeight > 20.0) ? 83.0 : 49.0)

//顶部安全距离
//#define  JPTopSafeHeight ((JP_IPhoneSize_X == YES || JP_IPhoneSize_Xr == YES || JP_IPhoneSize_Xs == YES || JP_IPhoneSize_Xs_Max == YES) ? 34.0 : 0.0)
//底部安全距离
#define  JPBottomSafeHeight (JPStateBarHeight > 20.0 ? 34.0 : 0.0)


//MARK: - 视图黄金比例
#define JPGoldenRatio    (0.618)

//MARK: - UIKit
#define FONT(size)             JPFONT(size)//使用地方太多，暂时无法做到全部移除
#define JPFONT(size)           [UIFont systemFontOfSize:[JPTool newFontSize:size]]

#define JPWindow              [UIApplication sharedApplication].delegate.window

#define JPUserDefults         [NSUserDefaults standardUserDefaults]


#define JP_RGBColor(r, g, b)    [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1.0]

/** 默认背景色 */
#define JP_BackGroundColor     JP_RGBColor(244, 244, 244)

/** 浅灰色线条颜色 */
#define JP_LineColor           JP_RGBColor(230, 230, 230)








