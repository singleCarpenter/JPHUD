//
//  JPBaseWebController.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/3/9.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPBaseWebController.h"
#import <Aspects/Aspects.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "UIImage+JPImage.h"

@interface JPBaseWebController ()

/*!
 自定义返回按钮
 */
@property (nonatomic ,strong) UIBarButtonItem * jp_popBarButtonItem;

@end

@implementation JPBaseWebController
 
- (instancetype)initWithURL:(NSURL *)URL
{
    self = [super initWithURL:URL];
    if (self) {
        self.hidesBottomBarWhenPushed = true;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
 
    [self jp_configure];
    
    [self jp_overloadNav];
}
-(void)jp_configure{
    
    //适配IphoneX
    if (@available(iOS 11.0, *)) {
        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    //设置背景色
    self.view.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    
}
-(void)jp_overloadNav{
    
    //隐藏内置的仿系统返回按钮
    self.navigationItem.hidesBackButton = YES;
 
    [self createCustomNavBackBotton];
 
    [self methodIntercept];
}
//拦截私有接口
-(void)methodIntercept{
    
    SEL updateNavigationItems = NSSelectorFromString(self.navigationType == AXWebViewControllerNavigationBarItem?@"updateNavigationItems":@"updateToolbarItems");
    
    NSError * error;
    
    @weakify(self);
    [self aspect_hookSelector:updateNavigationItems withOptions:AspectPositionAfter usingBlock:^(id data){
        @strongify(self);
        
        [self replacementPopButton];
        
    } error:&error];
    
}
//置换自定义返回按钮,IOP
-(void)replacementPopButton{
 
    self.navigationController.interactivePopGestureRecognizer.enabled = false;
    
    [self.navigationItem setLeftBarButtonItems:@[self.jp_popBarButtonItem] animated:false];
}
//创建自定义返回按钮
-(void)createCustomNavBackBotton{
    
    UIButton * popButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [popButton setImage:[UIImage imageNamedFromJPBundle:@"nav_back"] forState:UIControlStateNormal];
    [popButton sizeToFit];
    self.jp_popBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:popButton];
    
    @weakify(self);
    [[popButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(__kindof UIControl * _Nullable x) {
        @strongify(self);
        
        [self jp_popCurrentController];
    }];
    
}
//MARK: - 自定义返回按钮响应
-(void)jp_popCurrentController{
    
     [self.navigationController popViewControllerAnimated:true];
}

@end
