//
//  JPBaseTabBarController.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPBaseTabBarController : UITabBarController


-(void)addCustomChildController:(UIViewController *)controller title:(NSString *)title image:(UIImage*)image selectedImage:(UIImage*)selectedImage;

@end
