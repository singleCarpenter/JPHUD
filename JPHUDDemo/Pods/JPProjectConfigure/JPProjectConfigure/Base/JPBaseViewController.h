//
//  JPBaseViewController.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <HYBMasonryAutoCellHeight/UITableView+HYBCacheHeight.h>

@interface JPBaseViewController : UIViewController
<
UITableViewDelegate,
UITableViewDataSource,
DZNEmptyDataSetDelegate,
DZNEmptyDataSetSource
>

/**
 * @brief 页面数据大小
 *
 * @discussion 默认dataPageNum = 10
 */
@property (nonatomic ,assign)int dataPageNum DEPRECATED_MSG_ATTRIBUTE("Use pageSize: instead");


/*!
 * @brief 数据页面索引
 *
 * @discussion 默认dataPageIndex = 1
 */
@property (nonatomic ,assign)int dataPageIndex DEPRECATED_MSG_ATTRIBUTE("Use currentPage: instead");


/*!
 * @brief 数据页面索引
 *
 * @discussion 默认currentPage = 1
 */
@property (nonatomic ,assign)int currentPage;

/**
 * @brief 页面数据容量
 *
 * @discussion 默认pageSize = 10
 */
@property (nonatomic ,assign) NSInteger pageSize;

/**
 * @brief 数据总计分页，根据接口实际情况更正
 *
 * @discussion 默认totalPage = 1
 */
@property (nonatomic ,assign) NSInteger totalPage;

/**
 * 表格数据源
 */
@property (nonatomic ,strong ,readwrite)NSMutableArray * sourceArrayM;

/**
 * @brief 表格
 *
 * @discussion 默认Group样式，如需适应plain样式请使用(initWithTableStyle:)初始化控制器
 *
 */
@property (nonatomic ,strong ,readonly)UITableView * tableView;

/**
 * @brief 通知上级页面进行对应操作回调
 *
 * @discussion 当前控制器需要上级控制器同步进行相关操作
 */
@property (nonatomic ,copy) void (^currentControllerOperationBlock)(id data);



/**
 * @brief 构建自定义样式表格的控制器
 *
 * @param tableStyle 表格样式
 *
 * @return 当前控制器
 */
-(instancetype)initWithTableStyle:(UITableViewStyle)tableStyle DEPRECATED_MSG_ATTRIBUTE("如果需要指定plan样式表格请重载父类 jp_tableStyle:接口替换");


/**
 * @brief 配置当前控制器必要的信息
 *
 * @discussion 1. 调用时机：在视图控制器已加载视图层次到内存后调用(即在‘viewDidLoad’内部调用)
 *
 * @discussion 2. 调用顺序：jp_configure > jp_overloadNavItems > jp_setupUI > jp_requestData ->子类viewDidLoad
 */
-(void)jp_configure NS_REQUIRES_SUPER;


/**
 * @brief 重载当前控制器的导航栏
 *
 * @discussion 1. 调用时机：在视图控制器已加载视图层次到内存后调用(即在‘viewDidLoad’内部调用)
 *
 * @discussion 2. 调用顺序：jp_configure > jp_overloadNavItems > jp_setupUI > jp_requestData ->子类viewDidLoad
 */
-(void)jp_overloadNavItems NS_REQUIRES_SUPER;


/**
 * @brief 绘制控制器界面
 *
 * @discussion 1. 调用时机：在视图控制器已加载视图层次到内存后调用(即在‘viewDidLoad’内部调用)
 *
 * @discussion 2. 调用顺序：jp_configure > jp_overloadNavItems > jp_setupUI > jp_requestData ->子类viewDidLoad
 */
-(void)jp_setupUI NS_REQUIRES_SUPER;


/**
 * @brief 获取控制器需要的网络数据
 *
 * @discussion 1. 调用时机：在视图控制器已加载视图层次到内存后调用(即在‘viewDidLoad’内部调用)
 *
 * @discussion 2. 调用顺序：jp_configure > jp_overloadNavItems > jp_setupUI > jp_requestData ->子类viewDidLoad
 */
-(void)jp_requestData NS_REQUIRES_SUPER;


/**
 * @brief 是否需要隐藏导航栏
 *
 * @discussion 默认不隐藏，子类可以重写
 *
 * @return 是/否
 */
-(BOOL)jp_isNeedHiddenNavigationBar;

/**
 * @brief 配置当前默认表格样式
 *
 * @discussion 默认group样式，子类可以重写
 *
 * @return group/plan
 */
-(UITableViewStyle)jp_tableStyle;

@end
