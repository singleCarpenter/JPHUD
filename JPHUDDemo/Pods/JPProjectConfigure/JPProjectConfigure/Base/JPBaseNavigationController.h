//
//  JPBaseNavigationController.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JPBaseNavigationController : UINavigationController

/**
 * @brief 是否隐藏导航视图下发水平分割线
 *
 * @discussion 默认不隐藏。子类可以重写
 *
 * @return 隐藏/显示
 */
-(BOOL)jp_hiddenSegmentationLine;

/**
 主页面导航
 
 @param controller 根视图
 @param title 标题
 @return 主页面导航
 */
+(instancetype)jp_sharedNavControllerWithRoot:(UIViewController*)controller title:(NSString*)title;


@end
 







