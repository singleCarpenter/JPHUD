//
//  JPBaseCell.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <HYBMasonryAutoCellHeight/UITableViewCell+HYBMasonryAutoCellHeight.h>

@class JPBaseCell;
@protocol JPBaseCellDelegate <NSObject>

@optional

/**
 按钮上事件响应回调

 @param cell 单元格
 @param sender 事件响应者
 @param data 携带信息
 */
-(void)jp_tableViewCell:(JPBaseCell *)cell functionBtnDidClick:(id)sender data:(id)data;

@end

///按钮上事件响应回调
typedef void(^FunctionViewDidClickBlock)(id sender, id data);

@interface JPBaseCell : UITableViewCell


/**
 承载单元格的表格
 */
@property (nonatomic ,weak ,readonly) UITableView * tableView;

/*!
 单元格代理响应者
 */
@property (nonatomic ,weak) id<JPBaseCellDelegate> myDelegate;

/*!
 响应事件回调，与代理同级，选一实现
 */
@property (nonatomic ,copy) FunctionViewDidClickBlock functionViewDidClickBlock;


/**
 * @brief 快速创建单元格
 *
 * @discussion 默认单元格样式为‘UITableViewCellStyleDefault’，标识符为‘NSStringFromClass([self class]) + Identifier’
 *
 * @param tableView 加载中单元格的表格
 *
 * @return 单元格
 */
+(instancetype)jp_cellForTableView:(UITableView*)tableView;

/**
 * @brief 快速创建单元格
 *
 * @discussion 默认单元格样式为‘UITableViewCellStyleDefault’
 *
 * @param tableView 加载中单元格的表格
 *
 * @param identifier 单元格标识符
 *
 * @return 单元格
 */
+(instancetype)jp_cellForTableView:(UITableView*)tableView Identifier:(NSString*)identifier DEPRECATED_MSG_ATTRIBUTE("2.2.1版本废弃此接口，如使用请调用 jp_cellForTableView: identifier:,行为完全一致");
+(instancetype)jp_cellForTableView:(UITableView*)tableView identifier:(NSString*)identifier;

/**
 * @brief 快速创建单元格
 *
 * @discussion 默认标识符为‘NSStringFromClass([self class]) + Identifier’
 *
 * @param tableView 加载中单元格的表格
 *
 * @param style 单元格类型
 *
 * @return 单元格
 */
+(instancetype)jp_cellForTableView:(UITableView*)tableView style:(UITableViewCellStyle)style;

/**
 * @brief 快速创建单元格
 *
 * @param tableView 加载中单元格的表格
 *
 * @param identifier 单元格标识符
 *
 * @param style 单元格类型
 *
 * @return 单元格
 */
+(instancetype)jp_cellForTableView:(UITableView*)tableView Identifier:(NSString*)identifier style:(UITableViewCellStyle)style DEPRECATED_MSG_ATTRIBUTE("2.2.1版本废弃此接口，如使用请调用 jp_cellForTableView: identifier: style,行为完全一致");
+(instancetype)jp_cellForTableView:(UITableView*)tableView identifier:(NSString*)identifier style:(UITableViewCellStyle)style;

/**
 * @brief 配置当前视图必要的信息
 *
 * @discussion 1. 调用时机：在‘initWithStyle:reuseIdentifier:’内部
 *
 * @discussion 2. 调用顺序：jp_configure > jp_setupSubviews
 *
 */
-(void)jp_configure NS_REQUIRES_SUPER;

/**
 * @brief 布局视图
 *
 * @discussion 1. 调用时机：在‘initWithStyle:reuseIdentifier:’内部
 *
 * @discussion 2. 调用顺序：jp_configure > jp_setupSubviews
 *
 */
-(void)jp_setupSubviews NS_REQUIRES_SUPER;

/**
 个性化设置

 @param data 数据
 */
-(void)jp_setData:(id)data;

@end
