//
//  JPBaseNavigationController.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPBaseNavigationController.h"
#import "UIImage+JPImage.h"
@interface JPBaseNavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation JPBaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16],NSForegroundColorAttributeName:[UIColor colorWithWhite:0.2 alpha:1]}];
 
    self.interactivePopGestureRecognizer.delegate = self;
        
    self.navigationBar.translucent = false;
 
    if ([self jp_hiddenSegmentationLine] == true) {
        
        for (UIView * subview in self.navigationBar.subviews) {
            
            if ([subview isMemberOfClass:NSClassFromString(@"_UIBarBackground")]) {
                
                for (UIView * bg_subview in subview.subviews) {
                    
                    if ([bg_subview isMemberOfClass:NSClassFromString(@"UIImageView")] && bg_subview.frame.size.height < 1) {
                        
                        bg_subview.hidden = true;//找到线条隐藏
                    }
                    
                }
                
            }
            
        }
        
    }
 
}
-(BOOL)jp_hiddenSegmentationLine{
    return false;
}
//快速构建导航
+(instancetype)jp_sharedNavControllerWithRoot:(UIViewController*)controller title:(NSString*)title{
    
    JPBaseNavigationController * nav = [[JPBaseNavigationController alloc]initWithRootViewController:controller];
    
    
    controller.navigationItem.title = title;
    
    return nav;
}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [super pushViewController:viewController animated:animated];
    
    if (viewController != self.childViewControllers[0]) {
 
        viewController.hidesBottomBarWhenPushed = true;
        
        UIImage * backImageName = [UIImage imageNamedFromJPBundle:@"nav_back"];
        
        UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setImage:backImageName forState:UIControlStateNormal];
        [backBtn setImage:backImageName forState:UIControlStateHighlighted];
        [backBtn setImageEdgeInsets:UIEdgeInsetsMake(0, -15, 0, 15)];
        [backBtn sizeToFit];
        [backBtn addTarget:self action:@selector(navBarButtonClick) forControlEvents:UIControlEventTouchUpInside];
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        
    }else{
        viewController.hidesBottomBarWhenPushed = false;
    }
    
}
- (void)navBarButtonClick{
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:true];
    
    [self popViewControllerAnimated:true];
}
//MARK: - <UIGestureRecognizerDelegate>
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    return self.childViewControllers.count > 1 ? true : false;
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return  UIStatusBarStyleDefault;
}
//MARK: - 默认所有都不支持转屏,如需个别页面支持除竖屏外的其他方向，请在viewController重新下边这三个方法
// 是否支持自动转屏
- (BOOL)shouldAutorotate {
    return false;
}
// 支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


@end

 





