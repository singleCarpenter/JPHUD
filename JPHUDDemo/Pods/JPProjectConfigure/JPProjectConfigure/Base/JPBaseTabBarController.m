//
//  JPBaseTabBarController.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPBaseTabBarController.h"
#import "JPBaseNavigationController.h"

@interface JPBaseTabBarController ()<UITabBarControllerDelegate>

@end

@implementation JPBaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.delegate = self;
    
    self.tabBar.translucent = false;
    
}

//添加定制的子控制器
-(void)addCustomChildController:(UIViewController *)controller title:(NSString *)title image:(UIImage*)image selectedImage:(UIImage*)selectedImage{
    
    JPBaseNavigationController * nav = [JPBaseNavigationController jp_sharedNavControllerWithRoot:controller title:title];
 
    nav.tabBarItem.title = title;
    
    nav.tabBarItem.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];   //tabbar默认图片
    
    nav.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];//选中图片
    
    [self addChildViewController:nav];
    
}
//MARK - <UITabBarControllerDelegate>
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    
    [self tabBarButtonClick:[self getTabBarButton]];
    
}
//MARK: - 获取点击按钮
- (UIControl *)getTabBarButton{
    
    NSMutableArray *tabBarButtons = [[NSMutableArray alloc]initWithCapacity:0];
    
    for (UIView *tabBarButton in self.tabBar.subviews) {
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]){
            [tabBarButtons addObject:tabBarButton];
        }
    }
    UIControl *tabBarButton = [tabBarButtons objectAtIndex:self.selectedIndex];
    
    return tabBarButton;
}
//MARK: - 给按钮加帧动画
- (void)tabBarButtonClick:(UIControl *)tabBarButton{
    
    for (UIView *imageView in tabBarButton.subviews) {
        
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
        
        animation.keyPath = @"transform.scale";
        
        animation.values = @[@1.0,@0.7,@1.0];
        
        animation.duration = 0.2;
        
        animation.calculationMode = kCAAnimationCubic;
        
        [imageView.layer addAnimation:animation forKey:nil];
        
    }
    
    
}
//MARK: - 默认所有都不支持转屏,如需个别页面支持除竖屏外的其他方向，请在viewController重新下边这三个方法
// 是否支持自动转屏
- (BOOL)shouldAutorotate {
    return false;
}
// 支持哪些屏幕方向
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
