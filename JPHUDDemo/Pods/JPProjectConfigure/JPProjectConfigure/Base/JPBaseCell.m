//
//  JPBaseCell.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPBaseCell.h"
 
@interface JPBaseCell()

/**
 承载单元格的表格
 */
@property (nonatomic ,weak ,readwrite) UITableView * tableView;

@end

@implementation JPBaseCell

+(instancetype)jp_cellForTableView:(UITableView*)tableView{
    NSString * kCellDetailsIdentifier = [NSString stringWithFormat:@"%@Identifier",NSStringFromClass([self class])];
    return [self jp_cellForTableView:tableView identifier:kCellDetailsIdentifier style:UITableViewCellStyleDefault];
}
+(instancetype)jp_cellForTableView:(UITableView *)tableView style:(UITableViewCellStyle)style{
    NSString * kCellDetailsIdentifier = [NSString stringWithFormat:@"%@Identifier",NSStringFromClass([self class])];
    return [self jp_cellForTableView:tableView identifier:kCellDetailsIdentifier style:style];
}
+(instancetype)jp_cellForTableView:(UITableView *)tableView Identifier:(NSString *)identifier{
    return [self jp_cellForTableView:tableView identifier:identifier style:UITableViewCellStyleDefault];
}
+(instancetype)jp_cellForTableView:(UITableView *)tableView identifier:(NSString *)identifier{
    return [self jp_cellForTableView:tableView identifier:identifier style:UITableViewCellStyleDefault];
}
+(instancetype)jp_cellForTableView:(UITableView*)tableView Identifier:(NSString*)identifier style:(UITableViewCellStyle)style{
    
    return [self jp_cellForTableView:tableView identifier:identifier style:style];
}
+ (instancetype)jp_cellForTableView:(UITableView *)tableView identifier:(NSString *)identifier style:(UITableViewCellStyle)style{
 
    JPBaseCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
 
    if (cell == nil) {
        
        cell = [[self alloc] initWithStyle:style reuseIdentifier:identifier];
    }
    
    __weak typeof(tableView) weakTable = tableView;
    
    cell.tableView = weakTable;
    
    return cell;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self jp_configure];
        [self jp_setupSubviews];
    }
    return self;
}

-(void)jp_configure{
    self.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds = YES;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.imageView sizeToFit];
}
-(void)jp_setupSubviews{}
-(void)jp_setData:(id)data{}
@end
