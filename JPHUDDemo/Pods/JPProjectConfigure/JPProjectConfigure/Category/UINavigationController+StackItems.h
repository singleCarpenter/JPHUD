//
//  UINavigationController+StackItems.h
//  JPProjectConfigureDemo
//
//  Created by mac on 2018/6/8.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (StackItems)


/**
 移除导航栈堆中的某个控制器

 @param itemClass 控制器类名
 */
-(void)jp_removeStackItems:(Class)itemClass;


/**
 返回到栈堆中特定的控制器，加载动画
 
 @param vcClass 控制器类名
 */
-(void)jp_popToViewController:(Class)vcClass;

/**
 返回到栈堆中特定的控制器

 @param vcClass 控制器类名
 @param animated 是否加载动画
 */
-(void)jp_popToViewController:(Class)vcClass animated:(BOOL)animated;

@end
