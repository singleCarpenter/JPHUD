//
//  UIAlertController+JP.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/3/9.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^JPAlertCompentHandleBlock)(void);
typedef void(^JPActionSheetCompentHandleBlock)(NSInteger index);

@interface UIAlertController (JP)

/**
 *  快速创建Alert
 *
 *  @param message    提示的内容
 *  @param isCancel   是否显示取消按钮
 *  @param block      确认按钮点击回调
 *  @param controller 加载视图的控制器
 */
+(void)jp_showAlertWithMessage:(NSString *)message isCancel:(BOOL)isCancel successActionCommpleteHander:(JPAlertCompentHandleBlock)block showController:(UIViewController *)controller;

/**
 快速创建Alert

 @param message 提示的内容
 @param isCancel 是否显示取消按钮
 @param sureBlock 确认按钮点击回调
 @param cancleBlock 取消按钮点击回调（isCancel==true有效）
 @param controller 加载视图的控制器
 */
+(void)jp_showAlertWithMessage:(NSString *)message isCancel:(BOOL)isCancel successActionCommpleteHander:(JPAlertCompentHandleBlock)sureBlock cancleBlock:(JPAlertCompentHandleBlock)cancleBlock showController:(UIViewController *)controller;

/**
 快速创建ActionSheet

 @param message 提示的内容
 @param isCancel 是否显示取消按钮
 @param items 展示数组
 @param block 单元格点击回调
 @param controller 加载视图的控制器
 */
+(UIAlertController *)jp_showActionSheetWithMessage:(NSString *)message isCancel:(BOOL)isCancel items:(NSArray<NSString *> *)items successActionCommpleteHander:(JPActionSheetCompentHandleBlock)block showController:(UIViewController *)controller;


@end
