//
//  NSObject+Params.m
//  JPProjectConfigure+MVVMDemo
//
//  Created by mac on 2018/8/8.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "NSObject+Params.h"
#import <objc/runtime.h>


static const void * kParamsKey = &kParamsKey;

@implementation NSObject (Params)

-(NSDictionary *)jp_params{
    return objc_getAssociatedObject(self, kParamsKey);
}

-(void)setJp_params:(NSDictionary *)jp_params{
    objc_setAssociatedObject(self, kParamsKey, jp_params, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(instancetype)initWithParams:(NSDictionary *)params{
    
    if (self = [self init]) {
        [self setJp_params:params];
    }
    return self;
}

@end
