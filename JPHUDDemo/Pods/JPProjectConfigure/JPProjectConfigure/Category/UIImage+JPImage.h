//
//  UIImage+JPImage.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (JPImage)

/**
 * @brief 加载JPProjectConfigure.boundle图片
 *
 * @param name 图片名称
 *
 * @return 图片
 */
+ (UIImage *)imageNamedFromJPBundle:(NSString *)name;

@end
