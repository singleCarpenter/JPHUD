//
//  UINavigationController+StackItems.m
//  JPProjectConfigureDemo
//
//  Created by mac on 2018/6/8.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "UINavigationController+StackItems.h"

@implementation UINavigationController (StackItems)

-(void)jp_removeStackItems:(Class)itemClass{
 
    //接受栈堆中旧的控制器
    NSMutableArray * oldControllers = [NSMutableArray arrayWithArray:self.viewControllers];
    
    [self.viewControllers enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if ([obj isKindOfClass:[itemClass class]]) {
 
            [oldControllers removeObject:obj];
            
        }
    }];
    
    self.viewControllers = oldControllers.copy;
    
}
-(void)jp_popToViewController:(Class)vcClass{
    [self jp_popToViewController:vcClass animated:true];
}

-(void)jp_popToViewController:(Class)vcClass animated:(BOOL)animated{
    //返回易家宝主页
    __block BOOL had = false;
    for (UIViewController * controller in self.viewControllers) {
        
        if ([controller isKindOfClass:[vcClass class]]) {
            
            had = true;
            
            [self popToViewController:controller animated:true];
            
            break;
        }
    }
    
    if (had == false) {
        [self popToRootViewControllerAnimated:true];
    }
}

@end
