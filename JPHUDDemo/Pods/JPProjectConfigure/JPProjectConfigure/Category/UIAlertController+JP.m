//
//  UIAlertController+JP.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/3/9.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "UIAlertController+JP.h"

@implementation UIAlertController (JP)


+(void)jp_showAlertWithMessage:(NSString *)message isCancel:(BOOL)isCancel successActionCommpleteHander:(JPAlertCompentHandleBlock)block showController:(UIViewController *)controller{
    
    [self jp_showAlertWithMessage:message  isCancel:isCancel successActionCommpleteHander:block cancleBlock:nil showController:controller];
}

+(void)jp_showAlertWithMessage:(NSString *)message isCancel:(BOOL)isCancel successActionCommpleteHander:(JPAlertCompentHandleBlock)sureBlock cancleBlock:(JPAlertCompentHandleBlock)cancleBlock showController:(UIViewController *)controller{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (sureBlock) sureBlock();
    }]];
    
    if (isCancel) {
        [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (cancleBlock) {
                cancleBlock();
            }
        }]];
    }
    
    [controller?:[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alert animated:YES completion:nil];
    
}



+(UIAlertController *)jp_showActionSheetWithMessage:(NSString *)message isCancel:(BOOL)isCancel items:(NSArray<NSString *> *)items successActionCommpleteHander:(JPActionSheetCompentHandleBlock)block showController:(UIViewController *)controller{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:message message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [items enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:obj style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (block) {
                block(idx);
            }
        }]];
        
    }];
    
    if (isCancel) {
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
    }
    
    [controller?:[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:actionSheet animated:YES completion:nil];
    
    return actionSheet;
}


////选取照片
//+(void)hd_alertForChoosePhotoCameraCompHandleBlock:(CompentHandleBlock)cameraBlock photoLibrary:(CompentHandleBlock)photoLibrary showController:(UIViewController*)controller{
//
//    UIAlertController * at = [UIAlertController alertControllerWithTitle:@"请选择图片来源" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//    at.view.tintColor = kMainColor;
//
//    [at addAction:[UIAlertAction actionWithTitle:@"照相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
//            [JPHUD showError:@"我打不开照相机啊!"];
//            return ;
//        }
//        if (cameraBlock)cameraBlock();
//    }]];
//    [at addAction:[UIAlertAction actionWithTitle:@"从相册选取" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        if (photoLibrary) photoLibrary();
//
//    }]];
//    [at addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil]];
//
//    [controller?:[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:at animated:true completion:nil];
//
//}


@end
