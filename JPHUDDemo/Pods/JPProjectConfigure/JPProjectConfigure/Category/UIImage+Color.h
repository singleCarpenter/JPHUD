//
//  UIImage+Color.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

/**
 * @brief 根据颜色生成矩形图片
 *
 * @param color 待生成的图片颜色
 *
 * @return 自定义图片
 */
+(UIImage *)jp_imageWithColor:(UIColor *)color;

/**
 * @brief 根据颜色生成矩形图片
 *
 * @param color 待生成的图片颜色
 *
 * @param size 待生成图片尺寸
 *
 * @return 自定义图片
 */
+(UIImage *)jp_imageWithColor:(UIColor *)color size:(CGSize)size;


/**
 * @brief 根据颜色生成无边框圆角图片
 *
 * @param color 待生成的图片颜色
 *
 * @param size 待生成图片尺寸
 *
 * @param radius 待生成图片边角半径
 *
 * @return 自定义图片
 */
+ (UIImage *)jp_imageWithColor:(UIColor *)color size:(CGSize)size radius:(CGFloat)radius;

/**
 * @brief 根据颜色生成图片
 *
 * @param color 待生成的图片颜色
 *
 * @param size 待生成图片尺寸
 *
 * @param radius 待生成图片边角半径
 *
 * @param borderColor 待生成图片边框颜色
 *
 * @param borderWidth 待生成图片边框快递
 *
 * @return 自定义图片
 */
+ (UIImage *)jp_imageWithColor:(UIColor *)color size:(CGSize)size radius:(CGFloat)radius borderColor:(UIColor *)borderColor borderWidth:(CGFloat)borderWidth;

@end
