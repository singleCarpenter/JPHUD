//
//  NSObject+Params.h
//  JPProjectConfigure+MVVMDemo
//
//  Created by mac on 2018/8/8.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
 
@interface NSObject (Params)

/**
 去表征化参数列表
 */
@property (nonatomic ,strong ,readonly) NSDictionary * jp_params;

/**
 携带信息初始化VM对象
 */
-(instancetype)initWithParams:(NSDictionary *)params;

@end
