//
//  UIColor+Hex.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

/**
 * @brief 根据16进制色值生成对应颜色
 *
 * @param hex 16进制色值
 *
 * @return 颜色
 */
+ (UIColor *)jp_hexString:(NSString *)hex;

/**
 * @brief 根据16进制色值生成对应颜色
 *
 * @param hex 16进制色值
 *
 * @param alpha 透明度
 *
 * @return 颜色
 */
+ (UIColor *)jp_hexString:(NSString *)hex alpha:(CGFloat)alpha;


@end
