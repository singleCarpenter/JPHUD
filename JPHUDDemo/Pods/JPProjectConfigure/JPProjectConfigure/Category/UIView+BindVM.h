//
//  UIView+BindVM.h
//  DecorateDesigner
//
//  Created by mac on 2018/8/8.
//  Copyright © 2018年 zhuangdian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BindVM)

-(void)jp_bindViewModel:(id)viewModel;

@end
