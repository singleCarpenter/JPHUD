//
//  UIImage+JPImage.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "UIImage+JPImage.h"
 
@implementation UIImage (JPImage)

+ (UIImage *)imageNamedFromJPBundle:(NSString *)name{
    
    NSBundle *bundle = [NSBundle bundleForClass:NSClassFromString(@"JPBaseViewController")];
    NSURL *url = [bundle URLForResource:@"JPProjectConfigure" withExtension:@"bundle"];
    if (url) {
        bundle = [NSBundle bundleWithURL:url];
    };
    
    name = [name stringByAppendingString:@"@2x"];
    NSString *imagePath = [bundle pathForResource:name ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if (!image) {
        // 兼容业务方自己设置图片的方式
        name = [name stringByReplacingOccurrencesOfString:@"@2x" withString:@""];
        image = [UIImage imageNamed:name];
    }
    
    return image;
}

@end
