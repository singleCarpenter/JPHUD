//
//  JPNetManager.h
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <objc/runtime.h>


@interface JPNetManager : NSObject


/**
 快速创建网络会话对象

 @return 网络会话对象
 */
+ (AFHTTPSessionManager *)sharedAFSessionManager;

/**
 POST基本请求（对AFNetWork封装）

 @param path 请求路径
 @param params 参数
 @param completionHandle 响应信息
 */
+ (void)POST:(NSString *)path params:(NSMutableDictionary *)params completionHandle:(void (^)(id response, NSError * error))completionHandle;

/**
 GET基本请求（对AFNetWork封装）
 
 @param path 请求路径
 @param params 参数
 @param completionHandle 响应信息
 */
+ (void)GET:(NSString *)path params:(NSMutableDictionary *)params completionHandle:(void (^)(id response, NSError * error))completionHandle;


@end


@interface JPNetManager (RAC)

/**
 GET请求转换RAC信号
 
 @param path 请求路径
 @param parameters 参数
 @return signal
 */
+ (RACSignal *)rac_GET:(NSString *)path parameters:(NSDictionary *)parameters;


/**
 POST请求转换RAC信号
 
 @param path 请求路径
 @param parameters 参数
 @return signal
 */
+ (RACSignal *)rac_POST:(NSString *)path parameters:(NSDictionary *)parameters;



@end

