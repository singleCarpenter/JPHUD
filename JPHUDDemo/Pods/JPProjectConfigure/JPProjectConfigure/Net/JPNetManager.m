//
//  JPNetManager.m
//  JPProjectConfigureDemo
//
//  Created by Carpenter on 2018/2/6.
//  Copyright © 2018年 carpenter. All rights reserved.
//

#import "JPNetManager.h"


typedef NS_ENUM(NSInteger ,JPNetManagerMethodType) {
    ///get请求方式
    JPNetManagerMethodType_GET,
    ///post请求方式
    JPNetManagerMethodType_POST,
};

@implementation JPNetManager
 
+ (AFHTTPSessionManager *)sharedAFSessionManager{
    
    static AFHTTPSessionManager *manager =nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [AFHTTPSessionManager manager];
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        manager.requestSerializer.timeoutInterval = 20;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",@"text/json",@"text/javascript",@"text/plain",nil];
    });
    
    return manager;
}

+ (void)POST:(NSString *)path params:(NSMutableDictionary *)params completionHandle:(void (^)(id, NSError *))completionHandle{
    
    [[self sharedAFSessionManager] POST:path parameters:params progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (completionHandle) {
            completionHandle(responseObject, nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (completionHandle) {
            completionHandle(nil, error);
        }
    }];
}
+ (void)GET:(NSString *)path params:(NSMutableDictionary *)params completionHandle:(void (^)(id, NSError *))completionHandle{
    
    [[self sharedAFSessionManager] GET:path parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (completionHandle) {
            completionHandle(responseObject, nil);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (completionHandle) {
            completionHandle(nil, error);
        }
        
    }];
}
 


@end




@implementation JPNetManager (RAC)
 
+ (RACSignal *)rac_GET:(NSString *)path parameters:(NSDictionary *)parameters {
    
    return [[self rac_requestPath:path method:JPNetManagerMethodType_POST parameters:parameters] setNameWithFormat:@"<%@: %p> -rac_postPath: %@, parameters: %@", self.class, self, path, parameters];
}


+ (RACSignal *)rac_POST:(NSString *)path parameters:(NSDictionary *)parameters {
    
    return [[self rac_requestPath:path method:JPNetManagerMethodType_POST parameters:parameters] setNameWithFormat:@"<%@: %p> -rac_postPath: %@, parameters: %@", self.class, self, path, parameters];
}

+ (RACSignal *)rac_requestPath:(NSString *)path method:(JPNetManagerMethodType)method parameters:(NSDictionary *)parameters{
    
    @weakify(self);
    return [[RACSignal createSignal:^RACDisposable * _Nullable(id<RACSubscriber>  _Nonnull subscriber) {
        @strongify(self);
        switch (method) {
            case JPNetManagerMethodType_GET:
            {
                [self GET:path params:parameters.mutableCopy completionHandle:^(id response, NSError *error) {
                    
                    if (error) {
                        
                        [subscriber sendError:error];
                        
                    }else{
                        [subscriber sendNext:response];
                        [subscriber sendCompleted];
                    }
                    
                }];
            }
                break;
            case JPNetManagerMethodType_POST:
            {
                [self POST:path params:parameters.mutableCopy completionHandle:^(id response, NSError *error) {
                    
                    if (error) {
                        
                        [subscriber sendError:error];
                        
                    }else{
                        [subscriber sendNext:response];
                        [subscriber sendCompleted];
                    }
                    
                }];
 
            }
                break;
            default:
                break;
        }
 
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"信号被取消订阅了");
        }];
        
    }] takeUntil:[self rac_willDeallocSignal]];
}



@end
