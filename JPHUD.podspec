
Pod::Spec.new do |s|


  s.name         = "JPHUD"

  s.version      = "1.2.1"

  s.summary      = "修复远程链接"

  s.description  = <<-DESC
              一款自定义加载等待框
                   DESC

  s.homepage     = "https://gitee.com/singleCarpenter/JPHUD"

  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author       = { "Carpenter" => "158287481@qq.com" }

  s.platform     = :ios, "9.0"

  s.source       = { :git => "git@gitee.com:singleCarpenter/JPHUD.git", :tag => s.version }

  s.source_files  = "JPHUD/JPHUD.{h,m,bundle}"

  s.public_header_files = "JPHUD/JPHUD.h"

  s.resources = "JPHUD/*.bundle"

  s.frameworks = "UIKit", "Foundation"

  s.requires_arc = true

  s.dependency 'Masonry'
  s.dependency 'pop'

end
